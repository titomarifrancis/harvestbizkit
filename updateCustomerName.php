﻿<?php
require_once('template/header.php');
?>
			<div>
				<div class="panel panel-success">
					<div class="panel-heading"/>
                        <h3 class="panel-title">Update Customer Name</h3>
                    </div>
					<div class="panel-body">                    
<?php
require_once('dbconn.php');
$customerId= $_REQUEST['id'];
$sql = "select customer.id, customer.firstname, customer.midname, customer.lastname from customer where customer.id=$customerId";
$stmt= $dbh->query($sql);
$result= $stmt->fetchAll();
?>
						<form role="form" method="post" action="updateCustomerNameProcessor.php">
                        <input type="hidden" name="customerId" value="<?php echo $result[0][0];?>">
						<div class="form-group">
							<label for="fNameField">First Name</label>
							<input type="text" class="form-control" name="fNameField" id="fNameField" placeholder="Enter First Name" value="<?php echo $result[0][1];?>">
						</div>
						<div class="form-group">
							<label for="mNameField">Middle Name (Optional)</label>
							<input type="text" class="form-control" name="mNameField" id="mNameField" placeholder="Enter Middle Name" value="<?php echo $result[0][2];?>">
						</div>
						<div class="form-group">
							<label for="lNameField">Last Name</label>
							<input type="text" class="form-control" name="lNameField" id="lNameField" placeholder="Enter Last Name" value="<?php echo $result[0][3];?>">
						</div>
						<button type="submit" class="btn btn-primary">Update Name</button>
						</form>
					</div>
				</div>
			</div>
<?php
require_once('template/footer.php');