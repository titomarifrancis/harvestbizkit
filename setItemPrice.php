﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Set Item Sales Price</h3>
                </div>
				<div class="panel-body">
					<form role="form" method="post" action="itemPriceProcessor.php">
<?php
require_once('dbconn.php');

$sql= "select item.id, item.itemname, itemcosthistory.costvalue from item, itemcosthistory where itemcosthistory.itemid=item.id order by itemcosthistory.historytimestamp desc";
$stmt= $dbh->query($sql);
$result= $stmt->fetchAll();
?>						
						<div class="form-group">
							<label for="item">Select Item to Set Sales Price (Current Cost Indicated)</label>
							<select name="item" id="item" class="form-control">
<?php
foreach($result as $row)
{
?>
								<option value="<?php echo $row[0];?>"><?php echo "$row[1] : $row[2]";?></option>
<?php
}
?>						
							</select>
						</div>
						<div class="form-group">
							<label for="itemPrice">Item Price</label>
							<input type="text" class="form-control" name="itemPrice" id="itemPrice" placeholder="Enter Item Price">
						</div>
						<button type="submit" class="btn btn-default">Save</button>
					</form>
				</div>
			</div>
<?php
require_once('template/footer.php');