﻿<?php
$anotherJsonStr = '[
  {
    "products": [
      {
        "id": 1,
        "name": "1948 Porsche 356-A Roadster",
        "price": 53.9
      },
      {
        "id": 2,
        "name": "1948 Porsche Type 356 Roadster",
        "price": 62.16
      },
      {
        "id": 3,
        "name": "1949 Jaguar XK 120",
        "price": 47.25
      },
      {
        "id": 4,
        "name": "1952 Alpine Renault 1300",
        "price": 98.58
      },
      {
        "id": 5,
        "name": "1952 Citroen-15CV",
        "price": 72.82
      },
      {
        "id": 6,
        "name": "1956 Porsche 356A Coupe",
        "price": 98.3
      },
      {
        "id": 7,
        "name": "1957 Corvette Convertible",
        "price": 69.93
      },
      {
        "id": 8,
        "name": "1957 Ford Thunderbird",
        "price": 34.21
      },
      {
        "id": 9,
        "name": "1958 Chevy Corvette Limited Edition",
        "price": 15.91
      },
      {
        "id": 10,
        "name": "1961 Chevrolet Impala",
        "price": 32.33
      },
      {
        "id": 11,
        "name": "1962 LanciaA Delta 16V",
        "price": 103.42
      },
      {
        "id": 12,
        "name": "1965 Aston Martin DB5",
        "price": 65.96
      },
      {
        "id": 13,
        "name": "1966 Shelby Cobra 427 S/C",
        "price": 29.18
      },
      {
        "id": 14,
        "name": "1968 Dodge Charger",
        "price": 75.16
      },
      {
        "id": 15,
        "name": "1968 Ford Mustang",
        "price": 95.34
      },
      {
        "id": 16,
        "name": "1969 Chevrolet Camaro Z28",
        "price": 50.51
      },
      {
        "id": 17,
        "name": "1969 Corvair Monza",
        "price": 89.14
      },
      {
        "id": 18,
        "name": "1969 Dodge Charger",
        "price": 58.73
      },
      {
        "id": 19,
        "name": "1969 Dodge Super Bee",
        "price": 49.05
      },
      {
        "id": 20,
        "name": "1969 Ford Falcon",
        "price": 83.05
      },
      {
        "id": 21,
        "name": "1970 Chevy Chevelle SS 454",
        "price": 49.24
      },
      {
        "id": 22,
        "name": "1970 Dodge Coronet",
        "price": 32.37
      },
      {
        "id": 23,
        "name": "1970 Plymouth Hemi Cuda",
        "price": 31.92
      },
      {
        "id": 24,
        "name": "1970 Triumph Spitfire",
        "price": 91.92
      },
      {
        "id": 25,
        "name": "1971 Alpine Renault 1600s",
        "price": 38.58
      },
      {
        "id": 26,
        "name": "1972 Alfa Romeo GTA",
        "price": 85.68
      },
      {
        "id": 27,
        "name": "1976 Ford Gran Torino",
        "price": 73.49
      },
      {
        "id": 28,
        "name": "1982 Camaro Z28",
        "price": 46.53
      },
      {
        "id": 29,
        "name": "1982 Lamborghini Diablo",
        "price": 16.24
      },
      {
        "id": 30,
        "name": "1985 Toyota Supra",
        "price": 57.01
      },
      {
        "id": 31,
        "name": "1992 Ferrari 360 Spider red",
        "price": 77.9
      },
      {
        "id": 32,
        "name": "1992 Porsche Cayenne Turbo Silver",
        "price": 69.78
      },
      {
        "id": 33,
        "name": "1993 Mazda RX-7",
        "price": 83.51
      },
      {
        "id": 34,
        "name": "1995 Honda Civic",
        "price": 93.89
      },
      {
        "id": 35,
        "name": "1998 Chrysler Plymouth Prowler",
        "price": 101.51
      },
      {
        "id": 36,
        "name": "1999 Indy 500 Monte Carlo SS",
        "price": 56.76
      },
      {
        "id": 37,
        "name": "2001 Ferrari Enzo",
        "price": 95.59
      },
      {
        "id": 38,
        "name": "2002 Chevy Corvette",
        "price": 62.11
      }
    ],
    "name": "Classic Cars"
  },
  {
    "products": [
      {
        "id": 39,
        "name": "1936 Harley Davidson El Knucklehead",
        "price": 24.23
      },
      {
        "id": 40,
        "name": "1957 Vespa GS150",
        "price": 32.95
      },
      {
        "id": 41,
        "name": "1960 BSA Gold Star DBD34",
        "price": 37.32
      },
      {
        "id": 42,
        "name": "1969 Harley Davidson Ultimate Chopper",
        "price": 48.81
      },
      {
        "id": 43,
        "name": "1974 Ducati 350 Mk3 Desmo",
        "price": 56.13
      },
      {
        "id": 44,
        "name": "1982 Ducati 900 Monster",
        "price": 47.1
      },
      {
        "id": 45,
        "name": "1982 Ducati 996 R",
        "price": 24.14
      },
      {
        "id": 46,
        "name": "1996 Moto Guzzi 1100i",
        "price": 68.99
      },
      {
        "id": 47,
        "name": "1997 BMW F650 ST",
        "price": 66.92
      },
      {
        "id": 48,
        "name": "1997 BMW R 1100 S",
        "price": 60.86
      },
      {
        "id": 49,
        "name": "2002 Suzuki XREO",
        "price": 66.27
      },
      {
        "id": 50,
        "name": "2002 Yamaha YZR M1",
        "price": 34.17
      },
      {
        "id": 51,
        "name": "2003 Harley-Davidson Eagle Drag Bike",
        "price": 91.02
      }
    ],
    "name": "Motorcycles"
  },
  {
    "products": [
      {
        "id": 52,
        "name": "1900s Vintage Bi-Plane",
        "price": 34.25
      },
      {
        "id": 53,
        "name": "1900s Vintage Tri-Plane",
        "price": 36.23
      },
      {
        "id": 54,
        "name": "1928 British Royal Navy Airplane",
        "price": 66.74
      },
      {
        "id": 55,
        "name": "1980s Black Hawk Helicopter",
        "price": 77.27
      },
      {
        "id": 56,
        "name": "ATA: B757-300",
        "price": 59.33
      },
      {
        "id": 57,
        "name": "America West Airlines B757-200",
        "price": 68.8
      },
      {
        "id": 58,
        "name": "American Airlines: B767-300",
        "price": 51.15
      },
      {
        "id": 59,
        "name": "American Airlines: MD-11S",
        "price": 36.27
      },
      {
        "id": 60,
        "name": "Boeing X-32A JSF",
        "price": 32.77
      },
      {
        "id": 61,
        "name": "Corsair F4U ( Bird Cage)",
        "price": 29.34
      },
      {
        "id": 62,
        "name": "F/A 18 Hornet 1/72",
        "price": 54.4
      },
      {
        "id": 63,
        "name": "P-51-D Mustang",
        "price": 49.0
      }
    ],
    "name": "Planes"
  },
  {
    
    "products": [
      {
        "id": 64,
        "name": "18th century schooner",
        "price": 82.34
      },
      {
        "id": 65,
        "name": "1999 Yamaha Speed Boat",
        "price": 51.61
      },
      {
        "id": 66,
        "name": "HMS Bounty",
        "price": 39.83
      },
      {
        "id": 67,
        "name": "Pont Yacht",
        "price": 33.3
      },
      {
        "id": 68,
        "name": "The Mayflower",
        "price": 43.3
      },
      {
        "id": 69,
        "name": "The Queen Mary",
        "price": 53.63
      },
      {
        "id": 70,
        "name": "The Schooner Bluenose",
        "price": 34.0
      },
      {
        "id": 71,
        "name": "The Titanic",
        "price": 51.09
      },
      {
        "id": 72,
        "name": "The USS Constitution Ship",
        "price": 33.97
      }
    ],
    "name": "Ships"
  },
  {
    "products": [
      {
        "id": 73,
        "name": "1950's Chicago Surface Lines Streetcar",
        "price": 26.72
      },
      {
        "id": 74,
        "name": "1962 City of Detroit Streetcar",
        "price": 37.49
      },
      {
        "id": 75,
        "name": "Collectable Wooden Train",
        "price": 67.56
      }
    ],
    "name": "Trains"
  },
  {
    "products": [
      {
        "id": 76,
        "name": "1926 Ford Fire Engine",
        "price": 24.92
      },
      {
        "id": 77,
        "name": "1940 Ford Pickup Truck",
        "price": 58.33
      },
      {
        "id": 78,
        "name": "1940s Ford truck",
        "price": 84.76
      },
      {
        "id": 79,
        "name": "1954 Greyhound Scenicruiser",
        "price": 25.98
      },
      {
        "id": 80,
        "name": "1957 Chevy Pickup",
        "price": 55.7
      },
      {
        "id": 81,
        "name": "1958 Setra Bus",
        "price": 77.9
      },
      {
        "id": 82,
        "name": "1962 Volkswagen Microbus",
        "price": 61.34
      },
      {
        "id": 83,
        "name": "1964 Mercedes Tour Bus",
        "price": 74.86
      },
      {
        "id": 84,
        "name": "1980’s GM Manhattan Express",
        "price": 53.93
      },
      {
        "id": 85,
        "name": "1996 Peterbilt 379 Stake Bed with Outrigger",
        "price": 33.61
      },
      {
        "id": 86,
        "name": "Diamond T620 Semi-Skirted Tanker",
        "price": 68.29
      }
    ],
    "name": "Trucks and Buses"
  },
  {
    "products": [
      {
        "id": 87,
        "name": "18th Century Vintage Horse Carriage",
        "price": 60.74
      },
      {
        "id": 88,
        "name": "1903 Ford Model A",
        "price": 68.3
      },
      {
        "id": 89,
        "name": "1904 Buick Runabout",
        "price": 52.66
      },
      {
        "id": 90,
        "name": "1911 Ford Town Car",
        "price": 33.3
      },
      {
        "id": 91,
        "name": "1912 Ford Model T Delivery Wagon",
        "price": 46.91
      },
      {
        "id": 92,
        "name": "1913 Ford Model T Speedster",
        "price": 60.78
      },
      {
        "id": 93,
        "name": "1917 Grand Touring Sedan",
        "price": 86.7
      },
      {
        "id": 94,
        "name": "1917 Maxwell Touring Car",
        "price": 57.54
      },
      {
        "id": 95,
        "name": "1928 Ford Phaeton Deluxe",
        "price": 33.02
      },
      {
        "id": 96,
        "name": "1928 Mercedes-Benz SSK",
        "price": 72.56
      },
      {
        "id": 97,
        "name": "1930 Buick Marquette Phaeton",
        "price": 27.06
      },
      {
        "id": 98,
        "name": "1932 Alfa Romeo 8C2300 Spider Sport",
        "price": 43.26
      },
      {
        "id": 99,
        "name": "1932 Model A Ford J-Coupe",
        "price": 58.48
      },
      {
        "id": 100,
        "name": "1934 Ford V8 Coupe",
        "price": 34.35
      },
      {
        "id": 101,
        "name": "1936 Chrysler Airflow",
        "price": 57.46
      },
      {
        "id": 102,
        "name": "1936 Mercedes Benz 500k Roadster",
        "price": 21.75
      },
      {
        "id": 103,
        "name": "1936 Mercedes-Benz 500K Special Roadster",
        "price": 24.26
      },
      {
        "id": 104,
        "name": "1937 Horch 930V Limousine",
        "price": 26.3
      },
      {
        "id": 105,
        "name": "1937 Lincoln Berline",
        "price": 60.62
      },
      {
        "id": 106,
        "name": "1938 Cadillac V-16 Presidential Limousine",
        "price": 20.61
      },
      {
        "id": 107,
        "name": "1939 Cadillac Limousine",
        "price": 23.14
      },
      {
        "id": 108,
        "name": "1939 Chevrolet Deluxe Coupe",
        "price": 22.57
      },
      {
        "id": 109,
        "name": "1940 Ford Delivery Sedan",
        "price": 48.64
      },
      {
        "id": 110,
        "name": "1941 Chevrolet Special Deluxe Cabriolet",
        "price": 64.58
      }
    ],
    "name": "Vintage Cars"
  }
]';
echo "$anotherJsonStr<br/>";
//$jsonArray= json_decode($anotherJsonStr, true);
//print_r($jsonArray);