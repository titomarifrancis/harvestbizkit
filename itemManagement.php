﻿<?php
require_once('template/header.php');
require_once('dbconn.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Item Management</h3>
                </div>
				<div class="panel-body">
					<form role="form" method="post" action="itemProcessor.php">
						<div class="form-group">
							<label for="itemName">Item Name</label>
							<input type="text" class="form-control" name="itemName" id="itemName" placeholder="Enter Item Name">
						</div>
						<div class="form-group">
							<label for="itemCode">Item Code</label>
							<input type="text" class="form-control" name="itemCode" id="itemCode" placeholder="Enter Unique Item Come">
						</div>
<?php
//Item class selection dropdown
$q1= "select id, description from itemclass order by id asc";
$set= $dbh->query($q1);
$result= $set->fetchAll();
$resultSize= sizeof($result);

if($resultSize > 0)
{
?>
                        <div class="form-group">
							<label for="itemClass">Item Class</label>
							<select name="itemClass" id="itemClass" class="form-control">
<?php

    foreach($result as $rowData)
    {
?>
                                <option value="<?php echo $rowData[0];?>"><?php echo "$rowData[1]";?></option>
<?php
    }
    
?>    
							</select>
						</div>
<?php
}
?>						
						<button type="submit" class="btn btn-default">Save</button>
					</form>
				</div>
<?php
require_once('dbconn.php');

$query= "select id, itemName, itemCode from item order by itemName asc";

$stmt= $dbh->query($query);
$result= $stmt->fetchAll();

if(sizeof($result) > 0)
{
?>				
				<div class="list-group" role="navigation">
<?php
	foreach($result as $row)
	{
?>
					<a href="#" class="list-group-item"><strong><?php echo "$row[2] : $row[1]";?></strong></a>
<?php	
	}
?>
				</div>
<?php
}
?>				
			</div>
<?php
require_once('template/footer.php');