﻿<?php
require_once('template/header.php');
?>
			<div>
				<div class="panel panel-success">
					<div class="panel-heading"/>
                        <h3 class="panel-title">Update Customer Address</h3>
                    </div>
					<div class="panel-body">                    
<?php
require_once('dbconn.php');
$customerAddressId= $_REQUEST['id'];
$sql = "select customeraddress.doorfloorhousecompoundblocklot, customeraddress.streetname, brgyname.id, customeraddress.zonenumber, customeraddress.zipcode, customeraddress.customerid from customeraddress, brgyname where customeraddress.brgyname=brgyname.id and customeraddress.id='$customerAddressId'";
$stmt= $dbh->query($sql);
$result= $stmt->fetchAll();
?>
                        <form role="form" method="post" action="updateCustomerAddressProcessor.php">
                        <input type="hidden" name="customerId" value="<?php echo $result[0][5];?>">
                        <input type="hidden" name="customerAddressId" value="<?php echo $customerAddressId;?>">
						<div class="form-group">
							<label for="lNameField">Delivery Address</label>
							<input type="text" class="form-control" name="doorFloorHouseCompoundBlockLot" id="doorFloorHouseCompoundBlockLot" placeholder="Enter Door#, Floor#, House#, Compound Name, Block # and/or Lot#" value="<?php echo $result[0][0];?>">
							<input type="text" class="form-control" name="streetName" id="streetName" placeholder="Enter Street Name" value="<?php echo $result[0][1];?>">
							<input type="text" class="form-control" name="zoneNumber" id="zoneNumber" placeholder="Enter Zone Number" value="<?php echo $result[0][3];?>">
							<input type="text" class="form-control" name="zipCode" id="zipCode" placeholder="Enter Zip Code" value="<?php echo $result[0][4];?>">
						</div>
						<div class="form-group">
							<label for="countryName">Country</label>
							<select name="countryName" id="countryName" class="form-control">
								<option value="1">Philippines</option>
							</select>
						</div>
						<div class="form-group">
							<label for="townCityName">Town or City</label>
							<select name="townCityName" id="townCityName" class="form-control">
								<option value="1">Taguig City</option>
							</select>
						</div>
<?php
$sql2= "select id, brgyname from BrgyName where townCityId=1 order by brgyName asc";
$stmt2= $dbh->query($sql2);
$result2= $stmt2->fetchAll();
?>
						<div class="form-group">
							<label for="brgyName">Barangay Name</label>
							<select name="brgyName" id="brgyName" class="form-control">
<?php
foreach($result2 as $row2)
{
?>
								<option value="<?php echo $row2[0];?>" <?php if($row2[0] == $result[0][2]) echo "selected";?>><?php echo $row2[1];?></option>
<?php
}
?>
							</select>
						</div>						
						<button type="submit" class="btn btn-primary">Update Address</button>
						</form>
					</div>
				</div>
			</div>
<?php
require_once('template/footer.php');