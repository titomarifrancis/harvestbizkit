﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Send SMS by Barangay</h3>
                </div>
				<div class="panel-body">
                    <h4><strong>Reminder:</strong> DO NOT CLOSE THIS BROWSER WINDOW WHILE SENDING SMS</h4>
<?php
require_once('dbconn.php');
require_once('lib/smsApi.php');

$brgyId = $_REQUEST['brgyId'];
$smsMsgId = $_REQUEST['smsMsgId'];

$query= "select id, smsContent from smsMessages where id=$smsMsgId";
$stmt= $dbh->query($query);
$result= $stmt->fetchAll();
$smsMessageStr= $result[0][1];

$query2= "SELECT customercontact.phonenumber FROM customer, customercontact, customeraddress, brgyname, towncity, country WHERE customer.id=customercontact.customerid AND customeraddress.customerid=customer.id AND customeraddress.brgyname=brgyname.id AND brgyname.towncityid=towncity.id AND towncity.countryid=country.id AND brgyname.id=$brgyId order by customercontact.phonenumber asc";
$stmt2= $dbh->query($query2);
$result2= $stmt2->fetchAll();
$numRec= sizeof($result2);

$celContactNumCtr= 0;

$nonCelContactNumCtr= 0;

$smsMsgSentCtr=0;

$smsMsgFailCtr=0;

for($a=0; $a < $numRec; $a++)
{
    $custContactNum= $result2[$a][0];
    $custContactLen= strlen($custContactNum);
    $custContactXtr= substr($custContactNum, -10, 10);
    if(($custContactLen >= 11) && (is_numeric($custContactXtr)))
    {
        $custContactXtr= "0"."$custContactXtr";
        $celContactNumCtr++;
        
        //$smsUrl= "http://".$smsIP."/send/sms/".$custContactXtr."/".$smsMessageStr."";
        //echo "$smsUrl<br/>";
        
        /*
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $smsUrl);
        $response= curl_exec($ch);
        echo "navigate URL";
        curl_close($curl);
        */
        
        $response = sendSMS($custContactXtr, $smsMessageStr);

        
        if($response == 'OK')
        {
            $smsMsgSentCtr++;
        }
        else
        {
            $smsMsgFailCtr++;
        }
    }
    else
    {
        $nonCelContactNumCtr++;
    }
}
echo "Total Number of Contact Numbers: $numRec<br/>";
echo "Total Number of Valid Contact Numbers: $celContactNumCtr<br/>";
echo "Total Number of Successful SMS Message Sent: $smsMsgSentCtr<br/>";
echo "Total Number of Failed SMS Message Sent: $smsMsgFailCtr<br/>";
echo "Total Number of Invalid Contact Numbers: $nonCelContactNumCtr<br/>";
?>

                </div>
            </div>
<?php
require_once('template/footer.php');