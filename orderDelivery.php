﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Order Delivery</h3>
                </div>
				<div class="panel-body">
<?php

require_once('dbconn.php');
$customerId= $_REQUEST['customerid'];

$sql1 = "select distinct item.id from item intersect select item.id from item, itempricehistory where itempricehistory.itemid=item.id";
$stmt1= $dbh->query($sql1);
$result1= $stmt1->fetch();

if(sizeof($result1 > 0))
{
    foreach($result1 as $key => $value)
    {
        $sql2 = "select item.id, item.itemname, itempricehistory.pricevalue from item, itempricehistory where itempricehistory.itemid=item.id and item.id=$value order by historytimestamp desc limit 1";
        $stmt2= $dbh->query($sql2);
        $result2= $stmt2->fetch();
        $result2Size= sizeof($result2);
    }
}

?>
                    <form action="" method="post">
                        <table width='100%'>
                            <thead>
                                    <th width='25%'>Product</th>
                                    <th width='15%'>Price</th>
                                    <th width='5%'>Quantity</th>
                                    <th width='10%'> </th>
                                </tr>
                            </thead>
                            <tbody data-bind='foreach: lines'>
                                <tr>
                                    <td>
                                        <select data-bind='options: bizkitProductList, optionsText: "category", optionsCaption: "Select...", value: category'> </select>
                                    </td>
                                </tr>
                                <tr>            
                                    <td data-bind="with: category">
                                        <select data-bind='options: products, optionsText: "name", optionsCaption: "Select...", value: $parent.product'> </select>
                                    </td>            
                                    <td data-bind='with: product'>
                                        &nbsp;<span data-bind='text: formatCurrency(price)'> </span>
                                    </td>            
                                    <td>
                                        <input data-bind='visible: product, value: quantity, valueUpdate: "afterkeydown"' />
                                    </td>          
                                    <td>
                                        &nbsp;<span data-bind='visible: product, text: formatCurrency(subtotal())' ></span>
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger" data-bind='click: $parent.removeLine'>Remove</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                    <br/>
                    <p class='grandTotal'>
                        <strong>Total value: <span data-bind='text: formatCurrency(grandTotal())'> </span></strong>
                    </p>
                    <button type="button" class="btn btn-warning" data-bind='click: addLine'>Add product</button>
                    <button type="button" class="btn btn-success" data-bind='click: save'>Submit Order</button>
                </div>
            </div>
<script type="text/javascript">
<?php
function getcurrentpath()
{   $curPageURL = "";
    if ($_SERVER["HTTPS"] != "on")
            $curPageURL .= "http://";
     else
        $curPageURL .= "https://" ;
    if ($_SERVER["SERVER_PORT"] == "80")
        $curPageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
     else
        $curPageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
        $count = strlen(basename($curPageURL));
        $path = substr($curPageURL,0, -$count);
    return $path ;
}

$url= getcurrentpath();

require_once("productList.php");
?>

var bizkitProductList= <?php echo $json ?>;

function formatCurrency(value) {
    return "P" + value.toFixed(2);
}
 
var CartLine = function() {
    var self = this;
    self.category = ko.observable();
    self.product = ko.observable();
    self.quantity = ko.observable(1);
    self.subtotal = ko.pureComputed(function() {
        return self.product() ? self.product().price * parseInt("0" + self.quantity(), 10) : 0;
    });
 
    // Whenever the category changes, reset the product selection
    self.category.subscribe(function() {
        self.product(undefined);
    });
};

var Cart = function() {
    // Stores an array of lines, and from these, can work out the grandTotal
    var self = this;
    self.lines = ko.observableArray([new CartLine()]); // Put one line in by default
    self.grandTotal = ko.pureComputed(function() {
        var total = 0;
        $.each(self.lines(), function() { total += this.subtotal() })
        return total;
    });
 
    // Operations
    self.addLine = function() { self.lines.push(new CartLine()) };
    self.removeLine = function(line) { self.lines.remove(line) };
    self.save = function() {
        var dataToSave = $.map(self.lines(), function(line) {
            return line.product() ? {
                productId: line.product().id,
                productName: line.product().name,
                quantity: line.quantity(),
                subtotal: line.subtotal(),
                customerId: <?php echo $customerId;?>,
                encoder: <?php echo $loggedInUserId;?>
            } : undefined
        });
        //alert("Click on the hyperlink to " + JSON.stringify(dataToSave) + " confirm sales");
        var jsonHash= JSON.stringify(dataToSave);
        console.log(jsonHash);
        //var jsonDec= Base64.decode(jsonHash);
        //console.log(jsonDec);
        var url = "<?php echo $url;?>jsonProc.php?jsonFeed="+jsonHash;
        window.location=url;
    };
};
ko.applyBindings(new Cart());
</script>

<?php
require_once('template/footer.php');