﻿<?php
require_once('template/magic.php');
require_once('dbconn.php');

$contactPerson= strtoupper($_REQUEST['contactPerson']);
$compName= strtoupper($_REQUEST['compName']);
$supplierAddress= strtoupper($_REQUEST['supplierAddress']);
$contactNumber= $_REQUEST['contactNumber'];

try
{
	$dbh->beginTransaction();
	
	//insert data into supplier table
	$sql= "INSERT INTO supplier (contactPerson, companyName, address, contactNumber, creationDate, dataEncoder) VALUES (:contactPerson, :companyName, :address, :contactNumber, now(), :encoder)";
	$stmt= $dbh->prepare($sql);
	
	//the parameter passed for :encoder and dataEncoder will be replaced by the session user ID soon
	$stmt->execute(array(':contactPerson'=>$contactPerson, ':companyName'=>$compName, ':address'=>$supplierAddress, ':contactNumber'=>$contactNumber, ':encoder'=>$loggedInUserId));
	
	$dbh->commit();
}
catch(PDOException $e)
{
	$dbh->rollback();
	echo "Failed to complete transaction: " . $e->getMessage() . "\n";
	exit;
}

header("Location:$_SERVER[HTTP_REFERER]");