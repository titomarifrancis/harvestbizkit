﻿<?php
require_once('template/magic.php');
require_once('dbconn.php');

$smsMessage= $_REQUEST['smsMessage'];

try
{
	$dbh->beginTransaction();
	$sql= "INSERT INTO smsMessages (smsContent, creationDate, dataEncoder) VALUES ('$smsMessage', now(), $loggedInUserId)";
	$dbh->query($sql);
	
	$dbh->commit();
}
catch(PDOException $e)
{
	$dbh->rollback();
	echo "Failed to complete transaction: " . $e->getMessage() . "\n";
	exit;
}

header("Location:$_SERVER[HTTP_REFERER]");