﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Supplier Receipt Management</h3>
                </div>
				<div class="panel-body">
					<form role="form" method="post" action="supplierReceiptProcessor.php">
						<div class="form-group">
							<label for="receiptDate">Receipt Date</label>
							<input type="text" class="form-control" name="receiptDate" id="datepicker1" placeholder="Enter Receipt Date">
						</div>
						<div class="form-group">
							<label for="receiptNumber">Receipt #</label>
							<input type="text" class="form-control" name="receiptNumber" id="receiptNumber" placeholder="Enter Receipt Number">
						</div>
<?php
require_once('dbconn.php');

//Item class selection dropdown
$supQuery= "select id, companyName from supplier order by companyName asc";
$supStmt= $dbh->query($supQuery);
$supResult= $supStmt->fetchAll();
if(sizeof($supResult) > 0)
{
    echo "				<div class='form-group'>
							<label for='supplierId'>Supplier</label>
                            <select name='supplierId' id='supplierId' class='form-control'>";
    foreach($supResult as $supRow)
    {
        echo "                                <option value='$supRow[0]'>$supRow[1]</option>";
    }
    echo "					</select>
						</div>";
}
?>						
						<button type="submit" class="btn btn-default">Save</button>
					</form>
				</div>
<?php
$query= "select ItemSupplierCostRefDoc.id, ItemSupplierCostRefDoc.receiptDate, ItemSupplierCostRefDoc.receiptNumber, supplier.companyName, systemuser.realName from ItemSupplierCostRefDoc, supplier, systemuser where supplier.id=ItemSupplierCostRefDoc.supplierId and ItemSupplierCostRefDoc.isClosed=false and ItemSupplierCostRefDoc.isCleared=false and ItemSupplierCostRefDoc.isAudited=false and ItemSupplierCostRefDoc.dataEncoder=systemuser.id order by ItemSupplierCostRefDoc.receiptDate asc";

$stmt= $dbh->query($query);
$result= $stmt->fetchAll();

if(sizeof($result) > 0)
{
?>				
				<div class="list-group" role="navigation">
					<a href="#" class="list-group-item">
                        <div style="padding-bottom:20px; vertical-align:middle">
                            <div class="col-xs-12 col-md-3">
                                <strong>Receipt Date</strong>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <strong>Receipt #</strong>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <strong>Supplier Name</strong>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <strong>Data Encoder</strong>
                            </div>                            
                        </div>
                    </a>                
<?php
	foreach($result as $row)
	{
?>
					<a href="#" class="list-group-item">
                        <div style="padding-bottom:20px; vertical-align:middle">
                            <div class="col-xs-12 col-md-3">
                                <strong><?php echo "$row[1]";?></strong>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <?php echo "$row[2]";?>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <em><?php echo "$row[3]";?></em>
                            </div>
                            <div class="col-xs-12 col-md-3">
                                <?php echo "$row[4]";?>
                            </div>                            
                        </div>
                    </a>
<?php	
	}
?>
				</div>
<?php
}
?>				
			</div>
<?php
require_once('template/footer.php');