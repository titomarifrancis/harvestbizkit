﻿<?php
require_once('template/magic.php');
require_once('lib/secProc.php');
require_once('dbconn.php');

$realName = $_REQUEST['realName'];
$usrName = $_REQUEST['usrName'];
$passWd = $_REQUEST['passWd'];
$roleId = $_REQUEST['roleId'];

$spices = create_salt();
$magicWord = create_hash($passWd, $spices);

if((strlen($realName) > 0) && (strlen($usrName) > 0) && (strlen($passWd) > 0))
{
    try
    {
        $dbh->beginTransaction();
        $sql= "INSERT INTO systemUser (creationDate, realName, sysUserName, passWd, passwdSalt, roleId, dataEncoder) VALUES (now(), '$realName', '$usrName', '$magicWord', '$spices', '$roleId', $loggedInUserId)";
        $dbh->query($sql);
        
        $dbh->commit();
    }
    catch(PDOException $e)
    {
        $dbh->rollback();
        echo "Failed to complete transaction: " . $e->getMessage() . "\n";
        exit;
    }
}

header("Location:$_SERVER[HTTP_REFERER]");