﻿<?php
require_once('dbconn.php');

$itemClassDesc= strtoupper($_REQUEST['itemClassDesc']);

try
{
	$dbh->beginTransaction();
	$sql= "INSERT INTO itemclass (description, creationDate, dataEncoder) VALUES ('$itemClassDesc', now(), 1)";
	//echo $sql;
	//die();
	$dbh->query($sql);
	
	$dbh->commit();
}
catch(PDOException $e)
{
	$dbh->rollback();
	echo "Failed to complete transaction: " . $e->getMessage() . "\n";
	exit;
}

header("Location:$_SERVER[HTTP_REFERER]");