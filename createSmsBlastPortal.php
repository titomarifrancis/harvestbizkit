﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Create SMS Blass Message</h3>
                </div>
				<div class="panel-body">
					<form role="form" method="post" action="createSmsBlastMsgProcessor.php">
						<div class="form-group">
							<label for="brgyName">SMS Blast Message</label>
							<input type="text" class="form-control" name="smsMessage" id="smsMessage" placeholder="Enter SMS Blast Message">
						</div>
						<button type="submit" class="btn btn-default">Save</button>
					</form>
				</div>
<?php
require_once('dbconn.php');
$query= "select id, smsContent from smsMessages order by creationDate desc";
	
$stmt= $dbh->query($query);
$result= $stmt->fetchAll();
if(sizeof($result) > 0)
{
?>				
				<div class="list-group" role="navigation">
<?php
	foreach($result as $row)
	{
?>
					<a href="#" class="list-group-item"><strong><?php echo "$row[1]";?></strong></a>
<?php	
	}
?>
				</div>
<?php
}
?>
			</div>
<?php
require_once('template/footer.php');