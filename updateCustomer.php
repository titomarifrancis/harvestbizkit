<?php
require_once('template/header.php');
?>
			<div>
				<div class="panel panel-success">
					<div class="panel-heading">               
						<h3 class="panel-title">View and Update Customer Information</h3>
					</div>
					<div class="panel-body">
						<p>
							<strong>Customer Name&nbsp;:</strong><br/>                            
<?php
require_once('dbconn.php');

$profileId= $_REQUEST['profileid'];
$sql= "select customer.id, customer.firstname, customer.midname, customer.lastname, systemuser.realname from customer, systemuser where customer.dataencoder=systemuser.id and customer.id=$profileId";
$stmt= $dbh->query($sql);
$result= $stmt->fetch();

$url1="updateCustomerName.php?id=#";
if($loggedInAccessRole <= 3)
{
    $url1="updateCustomerName.php?id=$result[0]";
}
?>
<!--<a href="updateCustomerName.php?id=<?php echo $result[0];?>" rel="tooltip" title="Encoded by <?php echo $result[4];?>"><strong><?php echo "$result[3], $result[1] $result[2]";?></strong></a><br/>-->
<a href="<?php echo $url1;?>" rel="tooltip" title="Encoded by <?php echo $result[4];?>"><strong><?php echo "$result[3], $result[1] $result[2]";?></strong></a><br/>
                        <br/>

<?php
$sql2= "SELECT customeraddress.id, customeraddress.doorfloorhousecompoundblocklot, customeraddress.streetname, customeraddress.zoneNumber, brgyname.brgyname, towncity.towncityname, customeraddress.zipcode, country.countryname, systemuser.realname FROM customer, customeraddress, brgyname, towncity, country, systemuser WHERE customeraddress.customerid=customer.id AND customeraddress.brgyname=brgyname.id AND brgyname.towncityid=towncity.id AND towncity.countryid=country.id AND customeraddress.customerid=customer.id AND customeraddress.dataencoder=systemuser.id AND customer.id=$profileId ORDER BY customeraddress.id asc";
$stmt2= $dbh->query($sql2);
$result2= $stmt2->fetchAll();
$numResult2= sizeof($result2);


$sql3= "SELECT customercontact.id, customercontact.phonenumber, systemuser.realname FROM customer, customercontact, systemuser WHERE customer.id=customercontact.customerid AND customercontact.dataencoder=systemuser.id AND customer.id=$profileId order by customercontact.id asc";
$stmt3= $dbh->query($sql3);
$result3= $stmt3->fetchAll();
$numResult3= sizeof($result3);


?>					
							<strong>Delivery Address(es)&nbsp;:</strong><br/>
<?php
foreach($result2 as $row2)
{
    $url2="updateCustomerAddress.php?id=#";
    if($loggedInAccessRole <= 3)
    {
        $url2="updateCustomerAddress.php?id=$row2[0]";
    }
    echo "<a href='$url2' rel='tooltip' title='Encoded by $row2[8]'>$row2[1] $row2[2] Zone $row2[3] $row2[4] $row2[5] $row2[6] $row2[7]</a><br/>";
    
}
?>
<br/>							
							<strong>Contact Number(s)&nbsp;:</strong><br/>
<?php
foreach($result3 as $row3)
{
    $url3="updateCustomerContact.php?id=#";
    if($loggedInAccessRole <= 3)
    {
        $url3="updateCustomerContact.php?id=$row3[0]";
    }
    echo "<a href='$url3' rel='tooltip' title='Encoded by $row3[2]'>$row3[1]</a><br/>";
}
?>							
						</p>
                        <br/>
						<form role="form" method="post" action="addCustomerPhoneProcessor.php">
						<div class="form-group">
                            <input type="hidden" name="customerId" value="<?php echo $profileId;?>">
							<label for="contactNumber">Telephone or Cellphone Number</label>
							<input type="text" class="form-control" id="contactNumber" name="contactNumber" placeholder="Enter Telephone or Cellphone Number">
						</div>
                        <button type="submit" class="btn btn-primary">Add Contact Number</button>
                        </form>
                        <br/>                        
                        <form role="form" method="post" action="addCustomerAddressProcessor.php">
						<div class="form-group">
                            <input type="hidden" name="customerId" value="<?php echo $profileId;?>">
							<label for="lNameField">Delivery Address</label>
							<input type="text" class="form-control" name="doorFloorHouseCompoundBlockLot" id="doorFloorHouseCompoundBlockLot" placeholder="Enter Door#, Floor#, House#, Compound Name, Block # and/or Lot#">
							<input type="text" class="form-control" name="streetName" id="streetName" placeholder="Enter Street Name">
							<input type="text" class="form-control" name="zoneNumber" id="zoneNumber" placeholder="Enter Zone Number">
							<input type="text" class="form-control" name="zipCode" id="zipCode" placeholder="Enter Zip Code">
						</div>
						<div class="form-group">
							<label for="countryName">Country</label>
							<select name="countryName" id="countryName" class="form-control">
								<option value="1">Philippines</option>
							</select>
						</div>
						<div class="form-group">
							<label for="townCityName">Town or City</label>
							<select name="townCityName" id="townCityName" class="form-control">
								<option value="1">Taguig City</option>
							</select>
						</div>
<?php
$sql4= "select id, brgyname from BrgyName where townCityId=1 order by brgyName asc";
$stmt4= $dbh->query($sql4);
$result4= $stmt4->fetchAll();
?>
						<div class="form-group">
							<label for="brgyName">Barangay Name</label>
							<select name="brgyName" id="brgyName" class="form-control">
<?php
foreach($result4 as $row4)
{
?>
								<option value="<?php echo $row4[0];?>"><?php echo $row4[1];?></option>
<?php
}
?>
							</select>
						</div>						
						<button type="submit" class="btn btn-primary">Add Address</button>
						</form>
					</div>
				</div>
			</div>
<?php
require_once('template/footer.php');