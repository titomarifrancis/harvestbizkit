﻿<?php
require_once('dbconn.php');

$item= $_REQUEST['item'];
$itemSupplierReceipt= $_REQUEST['itemSupplierReceipt'];
$itemCost= $_REQUEST['itemCost'];
$itemQty= $_REQUEST['itemQty'];

try
{
	$dbh->beginTransaction();
	
	$sql= "INSERT INTO itemCostHistory(itemId, itemSupplierRefDocId, costValue, itemQty, historyTimestamp, dataEncoder) VALUES('$item', '$itemSupplierReceipt', '$itemCost', '$itemQty', now(), 1)";
	$dbh->query($sql);
	
	$dbh->commit();
}
catch(PDOException $e)
{
	$dbh->rollback();
	echo "Failed to complete transaction: " . $e->getMessage() . "\n";
	exit;
}

header("Location:$_SERVER[HTTP_REFERER]");