﻿<?php
require_once('template/magic.php');
require_once('dbconn.php');

$sqlSeq= "select start_value, last_value, is_called from customer_id_seq";
$stmtSeq= $dbh->query($sqlSeq);
$resultSeq= $stmtSeq->fetch();

$startValue= $resultSeq[0];
$lastValue= $resultSeq[1];
$isCalled= $resultSeq[2];

if($isCalled != TRUE)
{
	$nextId= $lastValue;
}
if($isCalled == TRUE)
{
	$nextId= $lastValue + 1;
}

$fNameField= strtoupper(addslashes($_REQUEST['fNameField']));
$mNameField= strtoupper(addslashes($_REQUEST['mNameField']));
$lNameField= strtoupper(addslashes($_REQUEST['lNameField']));
$contactNumber= $_REQUEST['contactNumber'];
$doorFloorHouseCompoundBlockLot= strtoupper($_REQUEST['doorFloorHouseCompoundBlockLot']);
$streetName= strtoupper(addslashes($_REQUEST['streetName']));
$zoneNumber= strtoupper($_REQUEST['zoneNumber']);
$brgyName= $_REQUEST['brgyName'];
$zipCode= $_REQUEST['zipCode'];
$customerNotes= addslashes($_REQUEST['customerNotes']);

date_default_timezone_set('UTC');

if((strlen($fNameField) > 0) || (strlen($lNameField) > 0) || (strlen($mNameField) > 0))
{
    try
    {
        $dbh->beginTransaction();

        $sql= "INSERT INTO customer (lastname, firstname, midname, creationdate, dataencoder) VALUES (:lName, :fName, :mName, now(), :encoder)";
        $stmt= $dbh->prepare($sql);
        
        //the parameter passed for :encoder and dataEncoder will be replaced by the session user ID soon
        $stmt->execute(array(':lName'=>$lNameField, ':fName'=>$fNameField, ':mName'=>$mNameField, ':encoder'=>$loggedInUserId));	
        
        //Allow recording of customer even if $contactNumber is empty
        //if(strlen($contactNumber) > 0)
        //{
            $sql2= "INSERT INTO customercontact (customerid, phonenumber, creationdate, dataencoder) VALUES ('$nextId', '$contactNumber', now(), $loggedInUserId)";
            $dbh->exec($sql2);
        //}

        if((strlen($doorFloorHouseCompoundBlockLot) > 0) || (strlen($streetName) > 0) || (strlen($zoneNumber) > 0) || (strlen($brgyName) > 0) || (strlen($zipCode) > 0))
        {
            $sql3= "INSERT INTO customeraddress (customerid, doorfloorhousecompoundblocklot, streetname, zonenumber, brgyname, zipcode, creationdate, dataencoder) VALUES ('$nextId', '$doorFloorHouseCompoundBlockLot', '$streetName', '$zoneNumber', '$brgyName', '$zipCode', now(), $loggedInUserId)";
            $dbh->exec($sql3);
        }
                
        if(strlen($customerNotes) > 0)
        {
            $sql4= "INSERT INTO customerNotes (customerId, noteEntry, creationDate, dataEncoder) VALUES ('$nextId', '".$customerNotes."', now(), $loggedInUserId)";
            $dbh->exec($sql4);
        }
        
        $dbh->commit();
    }
    catch(PDOException $e)
    {
        $dbh->rollback();
        echo "Failed to complete transaction: " . $e->getMessage() . "\n";
        exit;
    }
}

header("Location:$_SERVER[HTTP_REFERER]");