﻿<?php
require_once('template/magic.php');
?>
<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>BizKit Business Solutions</title>
	<meta name="description" content="">
	<link href='http://www.edgekit.com/favicon.ico' rel='icon' type='image/vnd.microsoft.icon'/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <script src="js/jquery-2.1.4.min.js"></script>
    <script src="js/jquery.base64.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/knockout-3.2.0.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script>
        $(function() {
            $( "#datepicker1" ).datepicker({ dateFormat: 'yy-mm-dd' });
            $( "#datepicker2" ).datepicker({ dateFormat: 'yy-mm-dd' });
        });
    </script>
</head>
<body>
	<!--[if lt IE 10]>
		<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
	<![endif]-->
	
	<!-- fixed navigation bar -->
	<div class="navbar  navbar-inverse" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#b-menu-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/bizkit2/home.php"><?php echo $clientName;?></a>
            </div>
            <div class="collapse navbar-collapse" id="b-menu-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="home.php">Home</a></li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown">Customer<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="addCustomer.php">New</a></li>
                            </ul>
                    </li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown">Inventory<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="notice.html">Reinforcement</a></li>
                                <li><a href="notice.html">Distribution</a></li>
                                <li><a href="notice.html">Replenishment</a></li>
                            </ul>
                    </li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown">Reports<b class="caret"></b></a>
                            <ul class="dropdown-menu">
<?php
if($loggedInAccessRole <= 4)
{
                                echo "<li><a href='customerSummaryReport.php'>Customer Summary</a></li>";
}
?>
                                <li><a href="notice.html">Gas Allowance Report</a></li>
                                <li><a href="notice.html">Sales</a></li>
                                <li><a href="notice.html">Cost</a></li>
                                <li><a href="notice.html">Price</a></li>
                            </ul>
                    </li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown">Administration<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <!--<li><a href="notice.html">System Configuration</a></li>-->
<?php
if($loggedInAccessRole <= 3)
{
                                echo "<li><a href='userManagement.php'>Users</a></li>";
                                echo "<li><a href='timesheetViewSetup.php'>Employee Timesheet Viewer</a></li>";
                                //echo "<li><a href='timesheetSummaryViewSetup.php'>Timesheet Summary Viewer</a></li>";
                                echo "<li><a href='supplierManagement.php'>Suppliers</a></li>";
}
?>
                                <!--<li><a href="townCityManagement.php">Town/City List</a></li>-->
                                <li><a href="brgyListManagement.php">Barangay List</a></li>

                                <!--<li><a href="itemClassManagement.php">Item Classification</a></li>-->
                                <li><a href="itemManagement.php">Products</a></li>

<?php
if($loggedInAccessRole <= 3)
{
                                echo "<li><a href='supplierReceiptManagement.php'>Supplier Receipt Entry</a></li>";    
                                echo "<li><a href='setItemCost.php'>Set Product Cost</a></li>";
                                echo "<li><a href='setItemPrice.php'>Set Product Price</a></li>";
}
?>
                                <!--<li><a href="addStore.php">Store Outlet/Branch</a></li>-->
                                <!--<li><a href="createSmsBlastPortal.php">Create SMS Blast Message</a></li>
                                <li><a href="sendSmsBlastPortal.php">Send SMS Blast Message</a></li>-->
                                <!--<li><a href="#">Gas Allowance Disbursement</a></li>-->
                            </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $loggedInUserRealname;?>&nbsp;<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="changeMyPassword.php">Change My Login Password</a></li>
                            <li><a href="myTimein.php">Time In</a></li>
                            <li><a href="myTimesheet.php">View My Timesheet</a></li>
                            <li><a href="myTimeout.php">Time Out</a></li>
                            <li><a href="logout.php">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div> <!-- /.nav-collapse -->
        </div> <!-- /.container -->
	</div> <!-- /.navbar -->
	
	<!-- main container -->
	<div class="container">
