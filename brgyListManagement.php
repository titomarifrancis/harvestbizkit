﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Barangay Name Listings</h3>
                </div>
				<div class="panel-body">
					<form role="form" method="post" action="brgyProcessor.php">
						<div class="form-group">
							<label for="brgyName">Barangay Name (currently covers Taguig City only)</label>
							<input type="text" class="form-control" name="brgyName" id="brgyName" placeholder="Enter Barangay Name">
						</div>
<?php
require_once('dbconn.php');
$sql2= "select id, towncityname from towncity order by towncityname asc";
$stmt2= $dbh->query($sql2);
$result2= $stmt2->fetchAll();
?>
						<div class="form-group">
							<label for="townCityId">Town/City</label>
							<select name="townCityId" id="townCityId" class="form-control">
<?php
foreach($result2 as $row)
{
?>
								<option value="<?php echo $row[0];?>"><?php echo $row[1];?></option>
<?php
}
?>
							</select>
						</div>						
						<button type="submit" class="btn btn-default">Save</button>
					</form>
				</div>
<?php
$query= "select id, brgyname from brgyname order by brgyname asc";
	
$stmt= $dbh->query($query);
$result= $stmt->fetchAll();
if(sizeof($result) > 0)
{
?>				
				<div class="list-group" role="navigation">
<?php
	foreach($result as $row)
	{
?>
					<a href="#" class="list-group-item"><strong><?php echo "$row[1]";?></strong></a>
<?php	
	}
?>
				</div>
<?php
}
?>
			</div>
<?php
require_once('template/footer.php');