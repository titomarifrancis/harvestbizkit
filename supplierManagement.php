﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Supplier Management</h3>
                </div>
				<div class="panel-body">
						<form role="form" method="post" action="supplierProcessor.php">
						<div class="form-group">
							<label for="fNameField">Contact Person</label>
							<input type="text" class="form-control" name="contactPerson" id="contactPerson" placeholder="Enter Contact Person Name">
						</div>
						<div class="form-group">
							<label for="mNameField">Company Name</label>
							<input type="text" class="form-control" name="compName" id="compName" placeholder="Enter Company Name">
						</div>
						<div class="form-group">
							<label for="mNameField">Address</label>
							<input type="text" class="form-control" name="supplierAddress" id="supplierAddress" placeholder="Enter Supplier Address">
						</div>						
						<div class="form-group">
							<label for="contactNumber">Contact Number</label>
							<input type="text" class="form-control" name="contactNumber" id="contactNumber" placeholder="Enter Supplier Contact Number">
						</div>
						<button type="submit" class="btn btn-default">Save</button>
						</form>
					</div>
<?php
require_once('dbconn.php');

//dislay all suppliers, serves as a supplier directory listing
$sql= "select * from supplier order by companyName asc";
$stmt= $dbh->query($sql);
$result= $stmt->fetchAll();

$supplierCount= sizeof($result);
if($supplierCount > 0)
{
?>
				<div class="list-group" role="navigation">
<?php
	foreach($result as $row)
	{
?>
					<a href="#" class="list-group-item"><em><?php echo $row[1];?></em><br/><strong><?php echo $row[2];?></strong><br/><?php echo $row[3];?><br/><?php echo $row[4];?></a>
<?php
	}
?>
				</div>
<?php
}
?>				
			</div>
<?php
require_once('template/footer.php');