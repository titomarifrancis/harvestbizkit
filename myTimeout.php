﻿<?php
require_once('template/magic.php');
require_once('dbconn.php');

try
{
    //check if user login flag is false;
    $sql1= "select id as EmployeeId, isLoggedIn from SystemUser where id='$loggedInUserId'";
    $stm1= $dbh->query($sql1);
    $result1= $stm1->fetch();
    $employeeId= $result1[0];
    $isLoggedIn= $result1[1];
    //echo "is logged in: $isLoggedIn";
    //echo "<br/>";

    if($isLoggedIn == 1)
    {
        $dbh->beginTransaction();
        $sql2= "insert into employeeLogout(creationDate, dataEncoder) VALUES(now(), '$loggedInUserId')";
        //echo "$sql2<br/>";
        $dbh->query($sql2);
        $sql3= "update systemuser set isLoggedIn=false where id='$employeeId'";
        //echo $sql3;
        //die();
        $dbh->query($sql3);
        $dbh->commit();
    }
}
catch(PDOException $e)
{
	$dbh->rollback();
	echo "Failed to complete transaction: " . $e->getMessage() . "\n";
	exit;
}
header("Location:$_SERVER[HTTP_REFERER]");