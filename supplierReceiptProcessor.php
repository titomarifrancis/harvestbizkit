﻿<?php
require_once('template/magic.php');
require_once('dbconn.php');

$receiptDate= $_REQUEST['receiptDate'];
$receiptNumber= $_REQUEST['receiptNumber'];
$supplierId= $_REQUEST['supplierId'];

try
{
	$dbh->beginTransaction();
	
	$sql= "INSERT INTO ItemSupplierCostRefDoc (receiptNumber, receiptDate, supplierId, creationDate, dataEncoder) VALUES (:receiptNumber, :receiptDate, :supplierId, now(), :encoder)";
	$stmt= $dbh->prepare($sql);
	
	//the parameter passed for :encoder and dataEncoder will be replaced by the session user ID soon
	$stmt->execute(array(':receiptNumber'=>$receiptNumber, ':receiptDate'=>$receiptDate, ':supplierId'=>$supplierId, ':encoder'=>$loggedInUserId));
	
	$dbh->commit();
}
catch(PDOException $e)
{
	$dbh->rollback();
	echo "Failed to complete transaction: " . $e->getMessage() . "\n";
	exit;
}

header("Location:$_SERVER[HTTP_REFERER]");