﻿<?php
require_once('lib/secProc.php');
$pass= $_REQUEST['pass'];
if(empty($pass))
{
    echo "Usage: URL/secPass.php?pass=PASSWORD STRING";
    exit();
}
$salt= create_salt();
$encryption= crypt($pass, $salt);
echo "password hash is $encryption salt is $salt<br/>";