﻿<?php
require_once('lib/secProc.php');

//connect to DB
require_once('dbconn.php');

// Sanitize incoming username and password
$username= $_REQUEST['usernameField'];
$password= $_REQUEST['passwordField'];

//verify from DB that username provided exists
$loginUsernameQuery= "SELECT id, passwd, passwdsalt, realname, roleid FROM systemuser WHERE sysusername='$username' AND issuspended=FALSE";
$stmt= $dbh->query($loginUsernameQuery);
$result= $stmt->fetchAll();

$userIdent= $result[0][0];
$userRealName= $result[0][3];
$dbProvPasswd= $result[0][1];
$dbProvPasswdSalt= $result[0][2];
$accessRoleId= $result[0][4];

if(validate_hash($password, $dbProvPasswd, $dbProvPasswdSalt))
{
    session_start();
    $_SESSION['userId']= $userIdent;
    $_SESSION['realName']= $userRealName;
    $_SESSION['accessRoleId']= $accessRoleId;
    
    $host  = $_SERVER['HTTP_HOST'];
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    $extra = 'home.php';

    //redirect to secured home page (home.php)
    header("Location:http://$host$uri/$extra");
}
else
{
    //redirect to index.php
    header("Location:$_SERVER[HTTP_REFERER]");
}