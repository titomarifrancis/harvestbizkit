﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Timesheet Viewer</h3>
                </div>
				<div class="panel-body">
					<form role="form" method="post" action="timesheetViewer.php">
						<div class="form-group">
							<label for="itemName">Employee Selector</label>
                            <select name="employeeId" id="employeeId" class="form-control">
<?php
require_once('dbconn.php');
$getEmployees = "select id, realname from systemuser where roleid > 3 and issuspended=false order by realname asc";

$stmt= $dbh->query($getEmployees);
$result= $stmt->fetchAll();
if(sizeof($result) > 0)
{
    foreach($result as $row)
	{
?>
                                <option value="<?php echo $row[0];?>"><?php echo $row[1];?></option>
<?php        
    }
    
}
?>
							</select>                            
						</div>
						<div class="form-group">
							<label for="itemCode">Timesheet Start Date</label>
							<input type="text" class="form-control" name="startDate" id="datepicker1" placeholder="Enter start date in yyyy-mm-dd format">
						</div>
						<div class="form-group">
							<label for="itemCode">Timesheet End Date</label>
							<input type="text" class="form-control" name="endDate" id="datepicker2" placeholder="Enter end date in yyyy-mm-dd format">
						</div>						
						<button type="submit" class="btn btn-default">View Timesheet</button>
					</form>
				</div>			
			</div>
<?php
require_once('template/footer.php');