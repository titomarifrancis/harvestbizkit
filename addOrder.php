<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
<?php
require_once('dbconn.php');

$customerId= $_REQUEST['customerid'];
$sql= "select firstname, midname, lastname from customer where id=$customerId";
$stmt= $dbh->query($sql);
$result= $stmt->fetch();
?>
                    <h3 class="panel-title"><strong><?php echo "$result[2], $result[0] $result[1]";?></strong>&nbsp;
<?php
if($loggedInAccessRole <= 4)
{
?>
                    <a class="btn btn-default right" href="updateCustomer.php?profileid=<?php echo $customerId;?>" role="button">View and Update Profile &raquo;</a>
<?php
}
?>
                    </h3>
                </div>
				<div class="panel-body">
					<div class="col-sm-6">
						<!-- box 1 -->
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title">Last Ordered Item(s) and Current Price</h3>
							</div>
							<div class="panel-body">
								<div class="list-group" role="navigation">
<?php
$sql2= "select saleshistory.id, saleshistory.receiptnumber, saleshistory.creationdate, systemuser.realname from saleshistory, systemuser where saleshistory.iscancelled=false and saleshistory.dataencoder=systemuser.id and saleshistory.customerid='$customerId' order by saleshistory.creationdate desc";
$stmt2= $dbh->query($sql2);
$result2= $stmt2->fetchAll();
$result2Size= sizeof($result2);
if($result2Size > 0)
{
    $latestOrderId= $result2[0][0];

    $sql3= "select salesdetails.id, salesdetails.itemid, item.itemname, (salesdetails.subtotal/salesdetails.itemqty) as salesprice from salesdetails, item where salesdetails.itemid=item.id and salesdetails.salesid='$latestOrderId' order by salesdetails.creationdate desc";
	$stmt3= $dbh->query($sql3);
	$result3= $stmt3->fetchAll();
	foreach($result3 as $row3)
	{
		$latestItemPriceId= $row3[1];

		//get latest price of a given item Id
		$sql4= "select priceValue from itempricehistory where itemId='$latestItemPriceId' order by id desc limit 1";
		$stmt4= $dbh->query($sql4);
		$result4= $stmt4->fetch();
?>
									<a class="list-group-item"><h1><div><strong><?php echo $row3[2];?></strong> :<br/><strong>P <?php echo $result4[0];?></strong></div></h1></a>
<?php
	}
}
else
{
?>
									<a class="list-group-item"><h1><div><strong>No transaction records found</strong></div></h1></a>
<?php
}
?>
								</div>
							</div>
						</div>

						<!-- box 2 -->
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">Order History (start from latest)</h3>
							</div>
							<div class="panel-body">
<?php
if($result2Size > 0)
{
	foreach($result2 as $row2)
	{
		$receiptNumber= $row2[1];
		$transactDate= $row2[2];
		//$branchName= $row2[3];
		$courier= $row2[3];
?>
										<a href="#" class="list-group-item">Transaction Date: <em><?php echo $transactDate;?></em><br/>Receipt#: <strong><?php echo $receiptNumber;?></strong><br/>Encoded by: <?php echo $courier;?></a>
<?php	
	}
}
else
{
?>
										<a href="addOrder.html" class="list-group-item">No transaction records found</a>
<?php
}
?>
							</div>
						</div>
					</div>
					
					<!-- column 2 -->
					<div class="col-sm-6">
						<!-- box 3 -->
						<div class="panel panel-info">
							<div class="panel-heading">
								<h3 class="panel-title">Click button to process order delivery</h3>
							</div>
							<div class="panel-body">
                                <p>
                                    <a class="btn btn-large btn-warning right" data-toggle="modal" href="orderDelivery.php?customerid=<?php echo "$customerId"; ?>">Order Delivery &raquo;</a>
                                </p>
							</div>
						</div>

						<!-- box 4 -->
						<div class="panel panel-warning">
							<div class="panel-heading">
								<h3 class="panel-title">Customer Notes (start from latest)</h3>
							</div>
							<div class="panel-body">
                                <form role="form" method="post" action="addCustomerNotes.php">
                                <div class="form-group">
                                    <input type="hidden" name="customerId" value="<?php echo $customerId;?>">
                                    <label for="customerNotes">Customer Note Entry</label>
                                    <textarea name="customerNotes" id="customerNotes" name="customerNotes" class="form-control"></textarea>
                                </div>
                                <button type="submit" class="btn btn-success">Add to Notes</button>
                                </form>
<?php
// get notes on this customer
$sql5= "select customernotes.noteentry, customernotes.creationdate, systemuser.realname from customernotes, systemuser where customerid='$customerId' and customernotes.dataencoder=systemuser.id order by customernotes.creationdate desc";
$stmt5= $dbh->query($sql5);
$result5= $stmt5->fetchAll();
$result5size= sizeof($result5);

if($result5size > 0)
{
	foreach($result5 as $row5)
	{
?>
								<p>
									<div class="list-group" role="navigation">
										<a href="addOrder.html" class="list-group-item">Entry Date: <?php echo $row5[1];?> by <?php echo $row5[2];?><br/><h3><?php echo $row5[0];?></h3></a>
									</div>							
								</p>
<?php
    }
}
else
{    
?>
								<p>
									<div class="list-group" role="navigation">
										<a href="" class="list-group-item">No customer notes found</a>
                                        
									</div>							
								</p>
<?php
}    
?>
							</div>
						</div>
					</div> <!-- /column 2 -->					
				</div>
			</div>			
<?php
require_once('template/footer.php');