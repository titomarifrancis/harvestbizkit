﻿<!DOCTYPE html>
<html lang="en">
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>BizKit Business Solutions</title>
	<meta name="description" content="">
	<link href='http://www.edgekit.com/favicon.ico' rel='icon' type='image/vnd.microsoft.icon'/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/custom.css">
	<script src="js/jquery-1.9.1.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/knockout-3.0.0.js"></script>    
</head>
<body>
	<!--[if lt IE 7]>
		<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
	<![endif]-->
	
	<!-- fixed navigation bar -->
	<div class="navbar  navbar-inverse" role="navigation">
	  <div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#b-menu-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/prototype/">A. Hernandez Marketing</a>
		</div>
			<div class="collapse navbar-collapse" id="b-menu-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a href="index.php">Home</a></li>
					<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown">Customer<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="addCustomer.php">New</a></li>
							</ul>
					</li>
					<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown">Inventory<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="notice.html">Reinforcement</a></li>
								<li><a href="notice.html">Distribution</a></li>
								<li><a href="notice.html">Replenishment</a></li>
							</ul>
					</li>
					<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown">Reports<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="customerSummaryReport.php">Customer Summary</a></li>
								<li><a href="notice.html">Sales</a></li>
								<li><a href="notice.html">Cost</a></li>
								<li><a href="notice.html">Price</a></li>
							</ul>
					</li>
					<li class="dropdown">
						<a href="" class="dropdown-toggle" data-toggle="dropdown">Administration<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="notice.html">System Configuration</a></li>
								<li><a href="notice.html">Users</a></li>
								<!--<li><a href="townCityManagement.php">Town/City List</a></li>-->
								<li><a href="brgyListManagement.php">Barangay List</a></li>
								<li><a href="supplierManagement.php">Suppliers</a></li>
								<!--<li><a href="itemClassManagement.php">Item Classification</a></li>-->
								<li><a href="itemManagement.php">Products</a></li>
								<li><a href="setItemCost.php">Set Product Cost</a></li>
								<li><a href="setItemPrice.php">Set Product Price</a></li>
								<li><a href="addStore.php">Store Outlet/Branch</a></li>
							</ul>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span><b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="#">Logout</a></li>
						</ul>
					</li>
				</ul>
			</div> <!-- /.nav-collapse -->
		</div> <!-- /.container -->
	</div> <!-- /.navbar -->
	
	<!-- main container -->
	<div class="container">
