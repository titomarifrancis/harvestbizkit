﻿<?php
require_once('template/magic.php');

$customerId= $_REQUEST['customerId'];
$customerNotes= addslashes($_REQUEST['customerNotes']);

require_once('dbconn.php');

if(strlen($customerNotes) > 0)
{
    try
    {
        $dbh->beginTransaction();

        $sql4= "INSERT INTO customerNotes (customerId, noteEntry, creationDate, dataEncoder) VALUES ($customerId, '".$customerNotes."', now(), $loggedInUserId)";
        $dbh->exec($sql4);

        $dbh->commit();
    }
    catch(PDOException $e)
    {
        $dbh->rollback();
        echo "Failed to complete transaction: " . $e->getMessage() . "\n";
        exit;
    }
}

header("Location:$_SERVER[HTTP_REFERER]");