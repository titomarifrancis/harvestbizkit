﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Set Item Cost</h3>
                </div>
				<div class="panel-body">
					<form role="form" method="post" action="itemCostProcessor.php">
<?php
require_once('dbconn.php');

$sql= "select item.id, item.itemName, itemClass.description from item, itemClass where itemclass.id=item.itemclassid order by item.itemName asc";
$stmt= $dbh->query($sql);
$result= $stmt->fetchAll();

//$sql2= "select id, companyName from supplier order by companyName asc";
$sql2= "select ItemSupplierCostRefDoc.id, ItemSupplierCostRefDoc.receiptDate, ItemSupplierCostRefDoc.receiptNumber, supplier.companyName, systemuser.realName from ItemSupplierCostRefDoc, supplier, systemuser where supplier.id=ItemSupplierCostRefDoc.supplierId and ItemSupplierCostRefDoc.isClosed=false and ItemSupplierCostRefDoc.isCleared=false and ItemSupplierCostRefDoc.isAudited=false and ItemSupplierCostRefDoc.dataEncoder=systemuser.id order by ItemSupplierCostRefDoc.receiptDate asc";
$stmt2= $dbh->query($sql2);
$result2= $stmt2->fetchAll();
?>						
						<div class="form-group">
							<label for="item">Select Item to Set Cost</label>
							<select name="item" id="item" class="form-control">
<?php
foreach($result as $row)
{
?>
								<option value="<?php echo $row[0];?>"><strong><?php echo $row[2];?></strong>&nbsp; : &nbsp;<?php echo $row[1];?></option>
<?php
}
?>
							</select>
						</div>
						<div class="form-group">
							<label for="itemSupplierReceipt">Select Item Supplier Receipt</label>
							<select name="itemSupplierReceipt" id="itemSupplierReceipt" class="form-control">							
<?php
foreach($result2 as $row2)
{
?>
								<option value="<?php echo $row2[0];?>"><strong><?php echo $row2[1];?></strong>&nbsp; : &nbsp;<?php echo $row2[2];?>&nbsp; : &nbsp;<?php echo $row2[3];?>&nbsp; : &nbsp;<?php echo $row2[4];?></option>
<?php
}
?>
							</select>
						</div>						
						<div class="form-group">
							<label for="itemQtyCost">Item Quantity</label>
							<input data-bind="value:itemQty" type="number" class="form-control" name="itemQty" id="itemQty" placeholder="Enter Item Quantity">
						</div>
						<div class="form-group">
							<label for="itemCost">Item Cost</label>
							<input data-bind="text:itemCost" type="number" step="any" min="0" class="form-control" name="itemCost" id="itemCost" placeholder="Enter Item Cost">
						</div>
						<button type="submit" class="btn btn-default">Save</button>
					</form>
				</div>
			</div>           
<?php
require_once('template/footer.php');