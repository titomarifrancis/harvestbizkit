﻿<?php
require_once('dbconn.php');

//get product class
$q1= "select id, description from itemclass order by id asc";
$st1= $dbh->query($q1);
$r1= $st1->fetchAll();
$r1Ctr = sizeof($r1);

$productList= array();
$prodCategoryName= array();

//get product and price per product class
for($x=0; $x < $r1Ctr; $x++)
{
    $itemClassId= $r1[$x][0];
    $itemClassName= $r1[$x][1];

    $q2= "select item.id, item.itemname from item where item.itemclassid=$itemClassId order by item.itemname asc";
    $st2= $dbh->query($q2);
    $r2= $st2->fetchAll();

    $resultSize = sizeof($r2);
    $products= Array();
    for($y=0; $y < $resultSize; $y++)
    {
        $itemId= $r2[$y][0];
        $itemName= $r2[$y][1];
                
        $q3= "select itempricehistory.pricevalue from item, itempricehistory where itempricehistory.itemid=item.id and item.id=$itemId order by itempricehistory.historytimestamp desc limit 1";
        $st3= $dbh->query($q3);
        $r3= $st3->fetch();

        $priceValueVar= $r3['pricevalue'];
        $priceValue= number_format($priceValueVar, 2);
        
        if($priceValue > 0.0)
        {
            $products[]=['id' => $itemId, 'name' => $itemName, 'price' => $priceValue];
        }
    }
    $productList[]=['products' => $products, 'category' => $itemClassName];
}
$json = json_encode($productList, 160);
echo $json;