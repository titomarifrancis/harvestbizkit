﻿<?php
$jsonHash= htmlspecialchars_decode($_REQUEST['jsonFeed']);
$orderObj= json_decode($jsonHash);
$orderObjSize= sizeof($orderObj);

require_once('dbconn.php');

$sqlSeq= "select start_value, last_value, is_called from saleshistory_id_seq";
$stmtSeq= $dbh->query($sqlSeq);
$resultSeq= $stmtSeq->fetch();

$startValue= $resultSeq[0];
$lastValue= $resultSeq[1];
$isCalled= $resultSeq[2];

if($isCalled != TRUE)
{
	$nextId= $lastValue;
}
if($isCalled == TRUE)
{
	$nextId= $lastValue + 1;
}

$branchId= 1;
$dateStr = date("Ymdhis");
$timeStamp = base64_encode(date("his"));
$receiptNum= "$dateStr-$branchId-$timeStamp";

$isSalesRecorded=0;

for($g=0; $g < $orderObjSize; $g++){
    $productId= $orderObj[$g]->productId;
    $productName= $orderObj[$g]->productName;
    $quantity= $orderObj[$g]->quantity;
    $subtotal= $orderObj[$g]->subtotal;
    $customerId= $orderObj[$g]->customerId;
    $encoder= $orderObj[$g]->encoder;
    
    try
    {
        $dbh->beginTransaction();

        if($isSalesRecorded == 0)
        {
            $recordSale= "insert into saleshistory(receiptnumber, customerid, branchid, creationdate, dataencoder) values('$receiptNum', '$customerId', '$branchId', now(), '$encoder')";
            $dbh->exec($recordSale);
            $isSalesRecorded=1;
        }        
        
        $recordSaleDetail= "insert into salesdetails(salesid, itemid, itemqty, subtotal, creationdate, dataencoder) values('$nextId', '$productId', '$quantity', '$subtotal', now(), '$encoder')";
        $dbh->exec($recordSaleDetail);
        $dbh->commit();        
    }
    catch(PDOException $e)
    {
        $dbh->rollback();
        echo "Failed to complete transaction: " . $e->getMessage() . "\n";
        exit;
    }    
}

header("Location:addOrder.php?customerid=$customerId");