﻿<?php
require_once('template/headerOut.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Please Login to Access Main Page</h3>
                </div>
				<div class="panel-body">
                    <div class="col-sm-6">
                            <form action="login.php" method="POST">
                                <div class="form-group">
                                    <input type="username" class="form-control" id="usernameField" name="usernameField" placeholder="Enter username">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" id="passwordField" name="passwordField" placeholder="Password">
                                </div>
                                <button type="submit" class="btn btn-primary">Log in</button>
                            </form>
                    </div>
                    <div class="col-sm-6">
                        <h3>Please contact your systems administrator to configure your username and password as login credentials to use this information system.</h3>
                    </div>
                </div>
            </div>            
<?php
require_once('template/footer.php');               