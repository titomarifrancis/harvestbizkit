﻿<?php
require_once('template/header.php');
?>
			<div>
				<div class="panel panel-success">
					<div class="panel-heading">               
						<h3 class="panel-title">Change My Login Password</h3>
					</div>
					<div class="panel-body">
						<form role="form" method="post" action="changeMyPasswordProcessor.php">
						<div class="form-group">
							<label for="myPasswd">New Password</label>
                            <input type="password" class="form-control" name="myPasswd" id="myPasswd" placeholder="New Password"/>
						</div>
						<div class="form-group">
							<label for="confirmPasswd">Confirm Password</label>
                            <input type="password" class="form-control" name="confirmPasswd" id="confirmPasswd" placeholder="Confirm Password"/>
						</div>                        
						<button type="submit" class="btn btn-primary">Change Password</button>
						</form>                        
					</div>                    
				</div>
			</div>
<?php
require_once('template/footer.php');                    