﻿<?php
require_once('dbconn.php');
require_once('template/magic.php');
require_once('lib/secProc.php');


$entry1= $_REQUEST['myPasswd'];
$entry2= $_REQUEST['confirmPasswd'];
$userId= $loggedInUserId;

if($entry1 == $entry2)
{
    $salt= create_salt();
    $encryption= crypt($entry1, $salt);    
    try
    {
        $dbh->beginTransaction();
        $sql= "UPDATE systemuser SET passwd='$encryption', passwdsalt='$salt' WHERE id='$userId'";
        //echo $sql;
        //die();
        $dbh->query($sql);
        
        $dbh->commit();
    }
    catch(PDOException $e)
    {
        $dbh->rollback();
        echo "Failed to complete transaction: " . $e->getMessage() . "\n";
        exit;
    }
}

header("Location:$_SERVER[HTTP_REFERER]");