﻿<?php
require_once('template/magic.php');
require_once('dbconn.php');

$brgyName= $_REQUEST['brgyName'];
$townCityId= $_REQUEST['townCityId'];

try
{
	$dbh->beginTransaction();
	$sql= "INSERT INTO brgyname (brgyname, townCityId, creationDate, dataEncoder) VALUES ('$brgyName', '$townCityId', now(), $loggedInUserId)";
	$dbh->query($sql);
	
	$dbh->commit();
}
catch(PDOException $e)
{
	$dbh->rollback();
	echo "Failed to complete transaction: " . $e->getMessage() . "\n";
	exit;
}

header("Location:$_SERVER[HTTP_REFERER]");