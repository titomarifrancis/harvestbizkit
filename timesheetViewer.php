﻿<?php
$employeeId= $_REQUEST['employeeId'];
$startDate= $_REQUEST['startDate'];
$endDate = $_REQUEST['endDate'];

require_once('dbconn.php');
$sql0 = "select realname from systemuser where id=$employeeId";
$stmt0= $dbh->query($sql0);
$result0 = $stmt0->fetch();
$employeeName = $result0[0];

$sqlI= "select * from employeeLogin where dataEncoder='$employeeId' and creationdate between '$startDate' and '$endDate' order by creationdate asc";
$stmtI= $dbh->query($sqlI);
$resultI= $stmtI->fetchAll();

$sqlO= "select * from employeeLogout where dataEncoder='$employeeId' and creationdate between '$startDate' and '$endDate' order by creationdate asc";
$stmtO= $dbh->query($sqlO);
$resultO= $stmtO->fetchAll();

require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Timesheet Viewer for <?php echo $employeeName;?></h3>
                </div>
				<div class="panel-body">
                    <table>
<?php
if(sizeof($resultI) == sizeof($resultO))
{
    require_once('config/tzparam.php');
    date_default_timezone_set($myTimeZone);
    $loopCtr= sizeof($resultI);
    echo "<table border='1'>";
    echo "<tr><td><h5 align='center'>Time in</h5></td><td><h5 align='center'>Time out</h5></td></tr>";
    for($a=0; $a < $loopCtr; $a++)
    {
        $loginTimestamp= strtotime($resultI[$a][1]);
        $loginDate= date("Y-m-d", $loginTimestamp);
        $loginTime= date("g:i:s A", $loginTimestamp);

        $logoutTimestamp= strtotime($resultO[$a][1]);
        $logoutDate= date("Y-m-d", $logoutTimestamp);
        $logoutTime= date("g:i:s A", $logoutTimestamp);
        
        echo "<tr><td><h5>&nbsp; $loginDate &nbsp; - &nbsp; $loginTime &nbsp;</h5></td><td><h5>&nbsp; $logoutDate &nbsp; - &nbsp; $logoutTime &nbsp;</h5></td></tr>";
        //echo "<tr><td><h5>$loginTime</h5></td><td><h5>$logoutTime</h5></td></tr>";
    }
    echo "</table></h1>";
}
else
{
    //As of 18 Apr 2015, this shouldn't happen
    echo "You have log in and out mismatch";
}

?>
                    </table>
                </div>
            </div>
<?php
require_once('template/footer.php');