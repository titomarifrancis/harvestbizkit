﻿<?php
$startDate= $_REQUEST['startDate'];
$endDate = $_REQUEST['endDate'];

require_once('dbconn.php');

$dayCount = "select '$endDate'::date - '$startDate'::date + 1";
$dayCtr= $dbh->query($dayCount);
$resultDayCtr = $dayCtr->fetch();
$numberOfTimesheetDays = $resultDayCtr[0];

$sql0 = "select id, realname from systemuser where roleid > 3 and issuspended=false";
$stmt0= $dbh->query($sql0);
$result0 = $stmt0->fetchAll();

require_once('template/header.php');
require_once('config/tzparam.php');
date_default_timezone_set($myTimeZone);
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Timesheet Summary Viewer</h3>
                </div>
				<div class="panel-body">
                    <table>
<?php
$resultCtr = sizeof($result0);
for($g=0; $g < $resultCtr; $g++)
{
    $employeeId = $result0[0][0];
    $realName = $result0[0][1];
    //echo "Real name $realName and employee ID $employeeId";

    $sqlI= "select * from employeeLogin where dataEncoder=$employeeId order by creationdate asc";
    $stmtI= $dbh->query($sqlI);
    $resultI= $stmtI->fetchAll();

    $sqlO= "select * from employeeLogout where dataEncoder=$employeeId order by creationdate asc";
    $stmtO= $dbh->query($sqlO);
    $resultO= $stmtO->fetchAll();    

    if(sizeof($resultI) == sizeof($resultO))
    {
        require_once('config/tzparam.php');
        date_default_timezone_set($myTimeZone);
        $loopCtr= sizeof($resultI);
        echo "<table border='1'>";
        echo "<tr><td><h5 align='center'>Time in</h5></td><td><h5 align='center'>Time out</h5></td></tr>";
        for($a=0; $a < $loopCtr; $a++)
        {
            $loginTimestamp= strtotime($resultI[$a][1]);
            $loginDate= date("Y-m-d", $loginTimestamp);
            $loginTime= date("H:i:s", $loginTimestamp);

            $logoutTimestamp= strtotime($resultO[$a][1]);
            $logoutDate= date("Y-m-d", $logoutTimestamp);
            $logoutTime= date("H:i:s", $logoutTimestamp);
            
            echo "<tr><td><h5>&nbsp; $loginDate &nbsp; - &nbsp; $loginTime &nbsp;</h5></td><td><h5>&nbsp; $logoutDate &nbsp; - &nbsp; $logoutTime &nbsp;</h5></td></tr>";
            //echo "<tr><td><h5>$loginTime</h5></td><td><h5>$logoutTime</h5></td></tr>";
        }
        echo "</table>";
    }
    else
    {
        echo "You have log in and out mismatch";
    }
}
?>
                    </table>
                </div>
            </div>
<?php
require_once('template/footer.php');