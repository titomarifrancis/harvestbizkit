﻿<?php
require_once('template/magic.php');

$customerId= $_REQUEST['customerId'];
$fNameField= strtoupper(addslashes($_REQUEST['fNameField']));
$mNameField= strtoupper(addslashes($_REQUEST['mNameField']));
$lNameField= strtoupper(addslashes($_REQUEST['lNameField']));

require_once('dbconn.php');

if((strlen($fNameField) > 0) || (strlen($lNameField) > 0) || (strlen($mNameField) > 0))
{
    try
    {
        $dbh->beginTransaction();

        $sql3= "UPDATE customer SET lastname='$lNameField', firstname='$fNameField', midname='$mNameField', modificationDate=now(), dataencoder='$loggedInUserId' WHERE id='$customerId'";
        $dbh->exec($sql3);

        $dbh->commit();
    }
    catch(PDOException $e)
    {
        $dbh->rollback();
        echo "Failed to complete transaction: " . $e->getMessage() . "\n";
        exit;
    }
}

header("Location:updateCustomer.php?profileid=$customerId");