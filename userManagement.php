﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">User Management</h3>
                </div>
				<div class="panel-body">
					<form role="form" method="post" action="userProcessor.php">
						<div class="form-group">
							<label for="realName">Complete Name</label>
							<input type="text" class="form-control" name="realName" id="realName" placeholder="Enter Complete Name">
						</div>
						<div class="form-group">
							<label for="usrName">User Name (one alphanumeric word starting with an alphabet without spaces)</label>
							<input type="text" class="form-control" name="usrName" id="usrName" placeholder="Enter Unique User Name">
						</div>
						<div class="form-group">
							<label for="passWd">Security Password</label>
							<input type="password" class="form-control" name="passWd" id="passWd" placeholder="Enter Password">
						</div>
						<div class="form-group">
							<label for="roleId">Role</label>
							<select name="roleId" id="roleId" class="form-control">
<?php
require_once('dbconn.php');
$getSysRoles= "SELECT id, roledescription FROM systemRole where id != 1 ORDER BY id desc";
$stmt= $dbh->query($getSysRoles);
$roleResult= $stmt->fetchAll();

foreach($roleResult as $rowRole)
{
?>
								<option value="<?php echo $rowRole[0];?>"><?php echo $rowRole[1];?></option>
<?php
}
?>
							</select>
						</div>
						<button type="submit" class="btn btn-default">Save</button>
					</form>
				</div>
<?php


$query= "SELECT id, realName, sysusername, isSuspended FROM systemUser WHERE roleid != 1 and isSuspended <> true ORDER by realName asc";
	
$stmt= $dbh->query($query);
$result= $stmt->fetchAll();
if(sizeof($result) > 0)
{
?>				
				<div class="list-group" role="navigation">
<?php
	foreach($result as $row)
	{
?>
					<a href="#" class="list-group-item"><strong><?php echo $row[1];?>&nbsp;/&nbsp;<?php echo $row[2];?></strong></a>
<?php	
	}
?>
				</div>
<?php
}
?>
			</div>
<?php
require_once('template/footer.php');