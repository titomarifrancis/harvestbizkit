﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
<?php
require_once('dbconn.php');
$brgyId= $_REQUEST['brgyid'];
$query= "select brgyname from brgyname where id= $brgyId";
$stmt= $dbh->query($query);
$result= $stmt->fetch();
?>

					<h3 class="panel-title"><strong><?php echo $result[0];?></strong> : Barangay Customer List Detail Report</h3>
                </div>
				<div class="panel-body">
<?php
$query2= "SELECT customer.id, customer.lastname, customer.firstname, customercontact.phonenumber, customeraddress.doorfloorhousecompoundblocklot, customeraddress.streetname, customeraddress.zoneNumber, brgyname.brgyname, towncity.towncityname, customeraddress.zipcode, country.countryname FROM customer, customercontact, customeraddress, brgyname, towncity, country WHERE customer.id=customercontact.customerid AND customeraddress.customerid=customer.id AND customeraddress.brgyname=brgyname.id AND brgyname.towncityid=towncity.id AND towncity.countryid=country.id AND brgyname.id=$brgyId order by customer.lastname asc";
$stmt2= $dbh->query($query2);
$result2= $stmt2->fetchAll();

if(sizeof($result2) > 0)
{
    //display the table headers
?>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Customer Name</th>
            <th>Contact Number</th>
            <th>Address</th>
        </tr>
    </thead>
    <tbody>
<?php    
    //display the content
    foreach($result2 as $row)
    {
?>
        <tr>
            <td><?php echo "$row[1], $row[2]";?></td>
            <td><?php echo $row[3]; ?></td>
            <td><?php echo "$row[4] $row[5] Zone $row[6] $row[7] $row[8] $row[9] $row[10]" ;?><br/><?php echo $row[11];?></td>
        </tr>
<?php        
    }
?>
    </tbody>
</table>
<?php
}
else
{
    echo "No data exists yet for this report";
}
?>
                </div>
            </div>
<?php
require_once('template/footer.php');