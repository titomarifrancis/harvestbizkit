﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">My Timesheet</h3>
                </div>
				<div class="panel-body">
                    <table>
<?php
require_once('dbconn.php');
$sqlI= "select * from employeeLogin where dataEncoder='$loggedInUserId' order by creationdate asc";
//$sqlI= "select * from employeeLogin where dataEncoder='$loggedInUserId' and and creationdate>='$startDate' and creationdate <='$endDate' order by creationdate asc";
$stmtI= $dbh->query($sqlI);
$resultI= $stmtI->fetchAll();

$sqlO= "select * from employeeLogout where dataEncoder='$loggedInUserId' order by creationdate asc";
//$sqlO= "select * from employeeLogout where dataEncoder='$loggedInUserId' and and creationdate>='$startDate' and creationdate <='$endDate' order by creationdate asc";
$stmtO= $dbh->query($sqlO);
$resultO= $stmtO->fetchAll();

if(sizeof($resultI) == sizeof($resultO))
{
    require_once('config/tzparam.php');
    date_default_timezone_set($myTimeZone);
    $loopCtr= sizeof($resultI);
    echo "<table border='1'>";
    echo "<tr><td><h5 align='center'>Time in</h5></td><td><h5 align='center'>Time out</h5></td></tr>";
    for($a=0; $a < $loopCtr; $a++)
    {
        $loginTimestamp= strtotime($resultI[$a][1]);
        $loginDate= date("Y-m-d", $loginTimestamp);
        $loginTime= date("g:i:s", $loginTimestamp);

        $logoutTimestamp= strtotime($resultO[$a][1]);
        $logoutDate= date("Y-m-d", $logoutTimestamp);
        $logoutTime= date("g:i:s", $logoutTimestamp);
        echo "<tr><td><h5>&nbsp; $loginDate &nbsp; - &nbsp; $loginTime &nbsp;</h5></td><td><h5>&nbsp; $logoutDate &nbsp; - &nbsp; $logoutTime &nbsp;</h5></td></tr>";
        //echo "<tr><td><h5>$loginDate - $loginTime</h5></td><td><h5>$logoutDate - $logoutTime</h5></td></tr>";
    }
    echo "</table></h1>";
}
else
{
    echo "You have log in and out mismatch";
}

?>
                    </table>
                </div>
            </div>
<?php
require_once('template/footer.php');