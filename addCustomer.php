<?php
require_once('template/header.php');
?>
			<div>
				<div class="panel panel-success">
					<div class="panel-heading">
						<h3 class="panel-title">Add New Customer (All fields are REQUIRED except those marked Optional)</h3>
					</div>
					<div class="panel-body">
						<form role="form" method="post" action="addCustomerProcessor.php">
						<div class="form-group">
							<label for="fNameField">First Name</label>
							<input type="text" class="form-control" name="fNameField" id="fNameField" placeholder="Enter First Name">
						</div>
						<div class="form-group">
							<label for="mNameField">Middle Name (Optional)</label>
							<input type="text" class="form-control" name="mNameField" id="mNameField" placeholder="Enter Middle Name">
						</div>
						<div class="form-group">
							<label for="lNameField">Last Name</label>
							<input type="text" class="form-control" name="lNameField" id="lNameField" placeholder="Enter Last Name">
						</div>
						<div class="form-group">
							<label for="contactNumber">Telephone or Cellphone Number</label>
							<input type="text" class="form-control" name="contactNumber" id="contactNumber" placeholder="Enter Telephone or Cellphone Number">
						</div>
						<div class="form-group">
							<label for="lNameField">Delivery Address</label>
							<input type="text" class="form-control" name="doorFloorHouseCompoundBlockLot" id="doorFloorHouseCompoundBlockLot" placeholder="Enter Door#, Floor#, House#, Compound Name, Block # and/or Lot#">
							<input type="text" class="form-control" name="streetName" id="streetName" placeholder="Enter Street Name">
							<input type="text" class="form-control" name="zoneNumber" id="zoneNumber" placeholder="Enter Zone Number">
							<input type="text" class="form-control" name="zipCode" id="zipCode" placeholder="Enter Zip Code">
						</div>
						<div class="form-group">
							<label for="countryName">Country</label>
							<select name="countryName" id="countryName" class="form-control">
								<option value="1">Philippines</option>
							</select>
						</div>
						<div class="form-group">
							<label for="townCityName">Town or City</label>
							<select name="townCityName" id="townCityName" class="form-control">
								<option value="1">Taguig City</option>
							</select>
						</div>
<?php
require_once('dbconn.php');

$sql2= "select id, brgyname from BrgyName where townCityId=1 order by brgyName asc";
$stmt2= $dbh->query($sql2);
$result2= $stmt2->fetchAll();
?>
						<div class="form-group">
							<label for="brgyName">Barangay Name</label>
							<select name="brgyName" id="brgyName" class="form-control">
<?php
foreach($result2 as $row2)
{
?>
								<option value="<?php echo $row2[0];?>"><?php echo $row2[1];?></option>
<?php
}
?>
							</select>
						</div>
						<div class="form-group">
							<label for="customerNotes">Customer Notes</label>
							<textarea name="customerNotes" id="customerNotes" class="form-control"></textarea>
						</div>
						<button type="submit" class="btn btn-primary">Add Customer</button>
						</form>
					</div>
				</div>
			</div>
<?php
require_once('template/footer.php');