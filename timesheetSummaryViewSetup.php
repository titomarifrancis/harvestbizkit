﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Timesheet Summary Viewer</h3>
                </div>
				<div class="panel-body">
					<form role="form" method="post" action="timesheetSummaryViewer.php">
						<div class="form-group">
							<label for="itemCode">Timesheet Start Date</label>
							<input type="text" class="form-control" name="startDate" id="datepicker" placeholder="Enter start date in yyyy-mm-dd format">
						</div>
						<div class="form-group">
							<label for="itemCode">Timesheet End Date</label>
							<input type="text" class="form-control" name="endDate" id="datepicker" placeholder="Enter end date in yyyy-mm-dd format">
						</div>						
						<button type="submit" class="btn btn-default">View Timesheet</button>
					</form>
				</div>			
			</div>
<?php
require_once('template/footer.php');