﻿<?php
require_once('header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Item Classification Management</h3>
                </div>
				<div class="panel-body">
					<form role="form" method="post" action="itemClassProcessor.php">
						<div class="form-group">
							<label for="itemClassDesc">Classification Description</label>
							<input type="text" class="form-control" name="itemClassDesc" id="itemClassDesc" placeholder="Enter Item Classification Description">
						</div>
						<button type="submit" class="btn btn-default">Save</button>
					</form>				
				</div>
<?php
require_once('dbconn.php');

//dislay all suppliers, serves as a supplier directory listing
$sql= "select id, description from itemclass order by description asc";
$stmt= $dbh->query($sql);
$result= $stmt->fetchAll();
//print_r($result);
//die();
$supplierCount= sizeof($result);
if($supplierCount > 0)
{
?>
				<div class="list-group" role="navigation">
<?php
	foreach($result as $row)
	{
?>
					<a href="#" class="list-group-item"><em><?php echo $row[1];?></em></a>
<?php				
	}
?>				
				</div>
<?php
}
?>				
			</div>
<?php
require_once('footer.php');