﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Customer Summary Report</h3>
                </div>
				<div class="panel-body">
                    <h4><strong>Reminder:</strong> Customers that have multiple address may appear more than once, depending on which barangay their address is located in</h4>
<?php
require_once('dbconn.php');

$query= "select distinct brgyname.id, brgyname.brgyname, count(customer.id) from customer, customeraddress, brgyname where customer.id=customeraddress.customerid and customeraddress.brgyname=brgyname.id group by brgyname.id order by brgyname.brgyname asc";
$stmt= $dbh->query($query);
$result= $stmt->fetchAll();

if(sizeof($result) > 0)
{
?>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Barangay Name</th>
            <th>Customer Count</th>
        </tr>
    </thead>
    <tbody>
<?php
    foreach($result as $row)
    {
?>
        <tr>
            <td><a href="brgyCustomerListDetail.php?brgyid=<?php echo $row[0];?>"><?php echo $row[1];?></a></td>
            <td><?php echo $row[2];?></td>
        </tr>
<?php        
    }
?>
    </tbody>
</table>
<?php    
}
else
{
    echo "No data exists yet for this report";
}
?>                
                </div>
            </div>
<?php
require_once('template/footer.php');