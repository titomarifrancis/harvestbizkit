﻿<?php
require_once('template/header.php');
?>
			<div class="panel panel-success">
                <div class="panel-heading">
					<h3 class="panel-title">Send SMS Blast Message</h3>
                </div>
				<div class="panel-body">
                    <h4><strong>Reminder:</strong> Please select the SMS message and barangay from the list below to send massive SMS message.</h4>
                    <form role="form" method="post" action="sendSmsBlastByBrgy.php">
<?php
require_once('dbconn.php');
$sql2= "select distinct brgyname.id, brgyname.brgyname from customer, customeraddress, brgyname where customer.id=customeraddress.customerid and customeraddress.brgyname=brgyname.id group by brgyname.id order by brgyname.brgyname asc";
$stmt2= $dbh->query($sql2);
$result2= $stmt2->fetchAll();
?>
						<div class="form-group">
							<label for="brgyId">Barangay Name</label>
							<select name="brgyId" id="brgyId" class="form-control">
<?php
foreach($result2 as $row)
{
?>
								<option value="<?php echo $row[0];?>"><?php echo $row[1];?></option>
<?php
}
?>
							</select>
						</div>
<?php
$query= "select id, smsContent from smsMessages order by creationDate desc";
	
$stmt= $dbh->query($query);
$result= $stmt->fetchAll();
?>                        
						<div class="form-group">
							<label for="smsMsgId">SMS Message</label>
							<select name="smsMsgId" id="smsMsgId" class="form-control">
<?php
foreach($result as $row2)
{
?>
								<option value="<?php echo $row2[0];?>"><?php echo $row2[1];?></option>
<?php
}
?>
							</select>
						</div>
						<button type="submit" class="btn btn-danger">Send SMS Message Blast</button>
					</form>
				</div>
<?php
require_once('template/footer.php');